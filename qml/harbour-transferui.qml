import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.TransferEngine 1.0
import org.nemomobile.transferengine 1.0

ApplicationWindow
{
    property TransferModel transferModel: TransferModel {}

    initialPage: Component { TransfersPage { } }


    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
